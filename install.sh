#!/usr/bin/env -S bash -e

# Cleaning the TTY
clear

# Selecting a kernel to install.
kernel_selector () {
    echo "List of kernels:"
    echo "1) Stable — Vanilla Linux kernel and modules, with a few patches applied"
    echo "2) Hardened — A security-focused Linux kernel"
    echo "3) Longterm — Long-term support (LTS) Linux kernel and modules"
    echo "4) Zen Kernel — Optimized for desktop usage"
    read -r -p "Enter the number of desired kernel: " choice
    case $choice in
        1 ) kernel="linux linux-headers"
            ;;
        2 ) kernel="linux-hardened linux-hardened-headers"
            ;;
        3 ) kernel="linux-lts linux-lts-headers"
            ;;
        4 ) kernel="linux-zen linux-zen-headers"
            ;;
        * ) echo "You did not enter a valid selection"
            kernel_selector
    esac
}

# Checking the microcode to install.
CPU=$(grep vendor_id /proc/cpuinfo)
if [[ $CPU == *"AuthenticAMD"* ]]; then
    microcode=amd-ucode
else
    microcode=intel-ucode
fi

# Checking if system is booted on UEFI or BIOS
[ -d /sys/firmware/efi ] && MODE="UEFI" || MODE="BIOS"
if [ $MODE == "UEFI" ]; then
    echo "Booted in UEFI mode"
    boot="grub efibootmgr"
else
    echo "Booted in Legacy BIOS mode"
    boot="grub"
fi

# Selecting the target for the installation
PS3="Select the disk where Arch Linux is going to be installed: "
select ENTRY in $(lsblk -dpnoNAME|grep -P "/dev/sd|nvme|vd");
do
    DISK=$ENTRY
    echo "Installing Arch Linux on $DISK"
    break
done

# Creating a new partition scheme.
echo "Creating new partition scheme on $DISK"
if [ $MODE == "UEFI" ]; then
    parted -s "$DISK" \
        mklabel gpt \
        mkpart ESP fat32 1MiB 257MiB \
        set 1 esp on \
        mkpart rootfs 257MiB 100% \
    
    ESP="/dev/disk/by-partlabel/ESP"
    rootfs="/dev/disk/by-partlabel/rootfs"
else
    parted -s "$DISK" \
        mklabel msdos \
        mkpart primary ext4 1MiB 100% \
        set 1 boot on \

    rootfs=$DISK"1"
fi

# Informing the Kernel of the changes.
echo "Informing the Kernel about the disk changes."
partprobe "$DISK"

# Formatting the ESP as FAT32.
if [ $MODE == "UEFI" ]; then
    echo "Formatting the EFI Partition as FAT32."
    mkfs.fat -F 32 $ESP &>/dev/null
fi

# Formatting the rootfs as EXT4.
echo "Formatting the rootfs Partition as EXT4."
mkfs.ext4 $rootfs &>/dev/null

# Mounting the newly created subvolumes.
echo "Mounting the newly created subvolumes."
mount $rootfs /mnt/

if [ $MODE == "UEFI" ]; then
    mkdir /mnt/boot/
    mkdir /mnt/boot/esp/
    mount $ESP /mnt/boot/esp/
fi

kernel_selector

# Enable parallel downloads
echo "Enabling parallel downloads"
sed -i 's/^#ParallelDownloads = 5/ParallelDownloads = 5/' /etc/pacman.conf

# Pacstrap (setting up a base sytem onto the new root)
echo "Installing the base system (it may take a while)"
pacstrap /mnt base base-devel $kernel $microcode linux-firmware $boot sudo vim git networkmanager

# Generating /etc/fstab
echo "Generating a new fstab."
genfstab -U /mnt >> /mnt/etc/fstab

# Setting hostname
read -r -p "Please enter the hostname: " hostname
echo "$hostname" > /mnt/etc/hostname

# Setting up locales
arch-chroot /mnt /usr/bin/vim /etc/locale.gen
read -r -p "Please insert the locale you use (format: xx_XX): " locale
echo "LANG=$locale.UTF-8" > /mnt/etc/locale.conf

# Setting up keyboard layout
read -r -p "Please insert the keyboard layout you use: " kblayout
echo "KEYMAP=$kblayout" > /mnt/etc/vconsole.conf

# Setting hosts file.
echo "Setting hosts file"
cat > /mnt/etc/hosts <<EOF
127.0.0.1   localhost
::1         localhost
127.0.1.1   $hostname.localdomain   $hostname
EOF

echo "Enabling NetworkManager"
systemctl enable NetworkManager --root=/mnt &>/dev/null

# Configuring the system.    
arch-chroot /mnt /bin/bash -e <<EOF
    
    # Setting up timezone
    ln -sf /usr/share/zoneinfo/$(curl -s http://ip-api.com/line?fields=timezone) /etc/localtime &>/dev/null
    
    # Setting up clock
    hwclock --systohc --utc

    # Generating locales
    echo "Generating locales"
    locale-gen &>/dev/null

    # Add post install script
    curl -o post.sh https://gitlab.com/Augis154/AIS/-/raw/main/post.sh
    chmod +x post.sh
EOF

    # Installing GRUB
    if [ $MODE == "UEFI" ]; then
        echo "Installing GRUB on /boot/efi"
        arch-chroot /mnt /usr/bin/grub-install --target=x86_64-efi --efi-directory=/boot/efi/ --bootloader-id=GRUB &>/dev/null
    else
        echo "Installing GRUB on $DISK"
        arch-chroot /mnt /usr/bin/grub-install $DISK
    fi
    
    # Creating grub config file
    echo "Creating GRUB config file"
    arch-chroot /mnt /usr/bin/grub-mkconfig -o /boot/grub/grub.cfg &>/dev/null

# Setting root password.
echo "Set your root password"
arch-chroot /mnt /bin/passwd

echo "Now you can reboot or chroot and make some changes"
echo "After reboot run ./post.sh to run post install script"
exit
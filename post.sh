#!/usr/bin/env bash

# Cleaning the TTY
clear

# Checking if system is booted on UEFI or BIOS
[ -d /sys/firmware/efi ] && MODE="UEFI" || MODE="BIOS"
if [ $MODE == "UEFI" ]; then
    echo "Booted in UEFI mode"
else
    echo "Booted in Legacy BIOS mode"
fi

# Getting system memory
MEM=$(free --giga | awk 'NR==2{printf "%s", $2 }')

if [ $MEM -gt 16 ]; then
    SWAP=$(($MEM/2))
elif [ $MEM -le 4 ]; then
    SWAP=$(($MEM*2))
else
    SWAP=$MEM
fi

read -r -p "Enter the size of swap file (recomended $SWAP) " size
if [[ $size != "" ]]; then
    SWAP=$size
fi

echo $SWAP"G"

# Creating a swap file
echo "Creating a swap file"
fallocate -l $SWAP"G" /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap sw 0 0' >> /etc/fstab

# Creating a regular user
read -r -p "Enter your username: " name
useradd -m -g users -G wheel -s /bin/bash $name
echo "Enter password of user $name"
passwd $name

# Edit the sudoers file
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
echo "Defaults Insults" >> /etc/sudoers

# Remove post install script from autorun
rm /etc/init.d/post.sh
rm /etc/rc.d/post.sh

exit